<?php
require __DIR__ . '/vendor/autoload.php';
use App\Providers\Bootstrap;
use App\Providers\Display;

$app = new Bootstrap();
$container = $app->start();
$receiptService = $container->get('ReceiptService');

try {
    $receipts = $receiptService->getReceipts();
    if (isset($receipts['error'])) {
        throw  new Exception($receipts['error']);
    }
    Display::render($receiptService->getDetails($receipts));
} catch (Exception $e) {
    echo $e->getMessage() . PHP_EOL;
}
