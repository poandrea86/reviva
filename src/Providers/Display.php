<?php

namespace App\Providers;


class Display
{
    public static function render($receipts) {
        try {
            if (count($receipts) > 0) {
                echo PHP_EOL;
                foreach ($receipts as $receipt) {
                    echo sprintf("Riepilogo per l'ordine %s", $receipt['name']) . PHP_EOL;
                    $saleTaxes = 0;
                    $importDuty = 0;
                    $total = 0;
                    foreach ($receipt['components'] as $component) {
                        if ($component['isTaxable'] && $component['isImported']) {
                            $component['saleTax'] = Display::rounded($component['saleTax']);
                            $component['importDuty'] = Display::rounded($component['importDuty']);
                            $component['totalCost'] = Display::rounded($component['totalCost']);
                        }
                        echo sprintf("%s %s: %s", $component['quantity'], $component['description'], $component['totalCost']) . PHP_EOL;
                        $saleTaxes += $component['saleTax'];
                        $importDuty += $component['importDuty'];
                        $total += $component['totalCost'];
                    }
                    echo sprintf("Sales Taxes: %s", round($saleTaxes + $importDuty, 2)) . PHP_EOL;
                    echo sprintf("Total: %s", round($total, 2)) . PHP_EOL;
                    echo  PHP_EOL;
                }

            } else {
                throw new \Exception("Receipts not found");
            }
        } catch (\Exception $e) {
            error_log($e->getFile() . " > " .$e->getMessage() . " at line " . $e->getLine() );
            return null;
        }
    }

    private static function rounded($number) : float {
        return number_format(round($number * 20,0) / 20,2,'.','');
    }
}
