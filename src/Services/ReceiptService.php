<?php

namespace App\Services;

use http\Exception;

class ReceiptService
{
    private $path;
    private $salesTax = 10;
    private $importDuty = 5;

    public function __construct()
    {
        $this->path = __DIR__ . '/../../storage';
    }

    public function getReceipts() : array {
        try {
            if (!file_exists($this->path)) {
                throw new \Exception('Storage path not exist');
            }
            $files = glob($this->path . '/*');
            if (count($files) === 0) {
                throw new \Exception('Receipts not found');
            }
            return collect($files)->filter(function ($item) {
                return ($item !== '.' OR $item !== '..') && strlen($item) > 0 && preg_match('/.csv/', $item);
            })->toArray();
        } catch (\Exception $e) {
            return ['error' => $e->getMessage()];
        }
    }

    public function getDetails(array $receipts) : array {
        $details = [];
        foreach ($receipts as $receipt) {
            $details[] = [
                'name' => basename($receipt),
                'components' => $this->readComponents($receipt)
            ];
        }
        return $details;
    }

    private function readComponents(string $receipt) : array
    {
        $components = [];
        $lines = file($receipt);
        foreach ($lines as $item) {
            $components[] = $this->getComponents($item);
        }
        return $components;
    }

    private function getComponents(string $item) : array {
        preg_match("~^(\d+)~", $item, $m);
        $quantity = (int) $m[1];
        $explode = explode(' ', $item);
        $price = (float) trim($explode[count($explode) - 1]);
        $description = trim(substr(preg_replace('/(' . $price . '|' . $quantity . ')/', '', $item), 0, -4));
        $isTaxable = $this->isTaxable($description);
        $isImported = $this->isImported($description);
        $cost = $quantity * $price;
        $importDuty = $this->getImportDuty($isImported, $cost);
        $saleTax = $this->getSaleTax($isTaxable, $cost);
        $totalCost = $cost + ($quantity * $saleTax) + $importDuty;
        return [
            'price' => $price,
            'quantity' => $quantity,
            'description' => $description,
            'saleTax' => $saleTax,
            'isTaxable' => $isTaxable,
            'isImported' => $isImported,
            'importDuty' => $importDuty,
            'totalCost' => round($totalCost, 2)
        ];
    }

    private function isTaxable(string $description) : bool {
        foreach ($this->notTaxableProduct() as $product) {
            if (preg_match('/' . $product . '/', $description)) {
                return false;
            }
        }
        return true;
    }

    private function isImported(string $description) : bool {
        if (preg_match('/imported/', $description)) {
            return true;
        }
        return false;
    }

    private function notTaxableProduct() : array {
        return ['book', 'chocolate', 'chocolates', 'headache pills'];
    }

    private function getSaleTax($isTaxable, $price) : float {
        return ($isTaxable) ? $price * ($this->salesTax / 100) : 0;
    }

    private function getImportDuty($isImported, $price) : float {
        return ($isImported) ? $price * ($this->importDuty / 100) : 0;
    }
}
