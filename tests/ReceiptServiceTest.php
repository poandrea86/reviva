<?php declare(strict_types=1);

use App\Providers\Display;
use App\Services\ReceiptService;
use PHPUnit\Framework\TestCase;

final class ReceiptServiceTest extends TestCase
{
    public function testStorageFolderExist() {
        $this->assertFileExists(__DIR__ . '/../storage');
    }

    public function testCanDisplayRender() {
        $receiptService = new ReceiptService();
        $receipts = $receiptService->getReceipts();
        $this->assertEmpty(Display::render($receiptService->getDetails($receipts)));
    }
}
